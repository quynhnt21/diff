**article**
- [Norsk forsker skaper strid om virusets opphav: – Dette viruset har ikke en naturlig opprinnelse](https://www.nrk.no/norge/norsk-forsker-skaper-strid-om-virusets-opphav_-_-dette-viruset-har-ikke-en-naturlig-opprinnelse-1.15043634)

**translated using google translate**
- [Norwegian scientist creates dispute over the origin of the virus: - This virus does not have a natural origin](https://translate.google.com/translate?hl=&sl=no&tl=en&u=https%3A%2F%2Fwww.nrk.no%2Fnorge%2Fnorsk-forsker-skaper-strid-om-virusets-opphav_-_-dette-viruset-har-ikke-en-naturlig-opprinnelse-1.15043634)

**the diff**  
- [view the diff in between the original and the updated version](https://gitlab.com/klevstul/diff/-/commit/7ed13a8156eddc35bba6385a6c9fabe61be12e99)
